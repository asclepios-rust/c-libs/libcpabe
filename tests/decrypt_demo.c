#include <glib.h>
#include <pbc.h>
#include <bswabe.h>

#include "src/cpabe.h"


#include "glib.h"
#include "stdio.h"
#include <sys/stat.h>
#include "common.h"
/*
// copied from lib/libcpabe/common.c
FILE*
fopen_read_or_die( char* file )
{
    FILE* f;

    if( !(f = fopen(file, "r")) )
        die("can't read file: %s\n", file);

    return f;
}
GByteArray*
suck_file( char* file )
{
    FILE* f;
    GByteArray* a;
    struct stat s;

    a = g_byte_array_new();
    stat(file, &s);
    g_byte_array_set_size(a, s.st_size);

    f = fopen_read_or_die(file);
    fread(a->data, 1, s.st_size, f);
    fclose(f);

    return a;
}*/
char*
suck_text_file( char* file )
{
    FILE* f;

    struct stat s;
    stat(file, &s);
    size_t len = s.st_size+1;
    char *data = malloc(sizeof(char) * s.st_size+1);

    f = fopen_read_or_die(file);
    fread(data, 1, s.st_size, f);
    fclose(f);

    data[len-1] = '\0';

    return data;
}

int main(int argc, char **argv)  {
    /// @todo implement

    // read demo data from "tests/res/"
    // see "tests/res/README.md" for more information
    char *b64_serialized_encrypted_SSEencKey = suck_text_file("tests/res/encrypted_SSEencKey.b64");
    char *b64_serialized_encrypted_SSEverKey = suck_text_file("tests/res/encrypted_SSEverKey.b64");
    char *b64_serialized_pubKey = suck_text_file(
                "tests/res/cpabe_public_key.james.b64");
                // "tests/res/c_generated_pub.b64");
                // "tests/res/cpabe_public_key.b64");
    char *b64_serialized_prvKey = suck_text_file("tests/res/cpabe_user_key (project:asclepiostestproject,role:owner).b64");

    //char *b64_nullterminated_serialized_pubKey = malloc(b64_serialized_pubKey.len+1);
    //memcpy(b64_nullterminated_serialized_pubKey,)

    // call the function to test
    char **keys = getSSEKeys(b64_serialized_encrypted_SSEencKey,
                             b64_serialized_encrypted_SSEverKey,
                             b64_serialized_pubKey,
                             b64_serialized_prvKey);

    // free data
    g_byte_array_unref(b64_serialized_encrypted_SSEencKey);
    g_byte_array_unref(b64_serialized_encrypted_SSEverKey);
    g_byte_array_unref(b64_serialized_pubKey);
    g_byte_array_unref(b64_serialized_prvKey);

    // looks like success...
    return 0;
}
