cmake_minimum_required(VERSION 3.5)

project(cpabe LANGUAGES C)

enable_testing()

find_package(PkgConfig REQUIRED)
pkg_check_modules(GLIB REQUIRED IMPORTED_TARGET glib-2.0)

include_directories(
    ${PROJECT_SOURCE_DIR}
    /usr/local/include/pbc
    ${GLIB_INCLUDE_DIRS}
    lib/libcpabe
)

set(HEADERS
    src/cpabe.h
    lib/libcpabe/common.h
)
set(SOURCES
    src/cpabe.c
    lib/libcpabe/common.c
)
# dynamic linkage
link_libraries(
    ${GLIB_LIBRARIES}
    bswabe
    pbc
    gmp
    crypto
)
# static linkage
#link_libraries(
#    libglib-2.0.a
#    libpthread.a
#    libc.a
#    libgcc.a
#    libpcre.a
#
#    libbswabe.a
#    /usr/local/lib/libpbc.a
#    libcrypto.a
#    libgmp.a
#    #--static
#)
#add_link_options(-ldl)

# the library to build
add_library(${PROJECT_NAME} STATIC ${SOURCES} ${HEADERS})

# tests
add_executable(test_decrypt_demo "tests/decrypt_demo.c" ${SOURCES} ${HEADERS})

add_custom_command(TARGET test_decrypt_demo POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E "copy_directory"
                       ${CMAKE_SOURCE_DIR}/tests/res/ $<TARGET_FILE_DIR:test_decrypt_demo>/tests/res/)

add_test(
    NAME decrypt_demo
    COMMAND test_decrypt_demo
)

