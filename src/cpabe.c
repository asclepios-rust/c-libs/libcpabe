#include "cpabe.h"
#include <glib.h>
#include <pbc.h>
#include "lib/libbswabe/bswabe.h"
#include "lib/libbswabe/private.h"
#include <stdbool.h>
#include <assert.h>
#include "common.h"
/** @brief read buffer in CPABE format (arbitrary format used in ASCLEPIOS and not yet documented anywhere) and extract AES and CPH buffer
 *  @param src  the buffer to read from
 */
/// @todo compare void read_cpabe_file( char* file, GByteArray** cph_buf, int* file_len, GByteArray** aes_buf ) in the CPABE library/cmmon.c
void read_cpabe_from_buffer( GByteArray *src,
                             GByteArray** cph_buf,
                             GByteArray** aes_buf )  {
    int i;
    int len;

    *cph_buf = g_byte_array_new();
    *aes_buf = g_byte_array_new();

    FILE *f = fmemopen(src->data, src->len, "r");

    // read aes buf
    len = 0;
    for( i = 3; i >= 0; i-- )
        len |= fgetc(f)<<(i*8);
    printf("reading %u bytes into aes_buf\n", len); fflush(stdout);
    g_byte_array_set_size(*aes_buf, len);
    fread((*aes_buf)->data, 1, len, f);

    // read cph buf
    len = 0;
    for( i = 3; i >= 0; i-- )
        len |= fgetc(f)<<(i*8);
    printf("reading %u bytes into cph_buf\n", len);  fflush(stdout);
    g_byte_array_set_size(*cph_buf, len);
    fread((*cph_buf)->data, 1, len, f);

    fclose(f);
}


char *cpabe_decrypt_buffer(bswabe_pub_t *pub, bswabe_prv_t *prv, GByteArray *cpabe_buffer)  {

    GByteArray *cph_buf=NULL;
    GByteArray *aes_buf=NULL;
    read_cpabe_from_buffer(cpabe_buffer, &cph_buf, &aes_buf);

    printf("cph_buf: ");
    for(int i=0; i<cph_buf->len; i++)
        printf("%x ", cph_buf->data[i]);
    printf("\n");
    fflush(stdout);

    printf("ciphertext (aes_buf): >%s<, hex: ", aes_buf->data);
    for(int i=0; i<aes_buf->len; i++)
        printf("%x ",  aes_buf->data[i] );
    printf(", size=%i", aes_buf->len);
    printf("\n");
    fflush(stdout);

    bswabe_cph_t *cph = bswabe_cph_unserialize(pub, cph_buf, true);  // frees cph_buf

    element_t beb;
    if( ! bswabe_dec(pub, prv, cph, beb) )
        /// @todo do not die but throw exception?
        die("%s", bswabe_error());  // decryption has failed (e.g. bad private key)
    bswabe_cph_free(cph);








    GByteArray *plt = aes_128_cbc_decrypt(aes_buf, beb);
    /// @todo g_byte_array_set_size(plt, file_len);
    g_byte_array_free(aes_buf, 1);

    /// @todo spit_file(out_file, plt, 1);
    printf("plaintext: >%s<, hex: ", plt->data);
    for(int i=0; plt->data[i]!='\0'; i++)
        printf("%x ",  plt->data[i] );
    printf(", size=%i", strlen(plt->data));
    printf("\n");
    fflush(stdout);

    /// @todo analyze element
    // if that element is true:
        // plt = AESCoder.decrypt(beb.e.toBytes(), aesBuf);  // plt is the result of Cpabe::dec (including newline characters)
    // else
        // System.exit(0);  // erm, what, why?*/
}

char **getSSEKeys(char *b64_encrypted_SSEencKey,
                  char *b64_encrypted_SSEverKey,
                  char *b64_serialized_pubKey,
                  char *b64_serialized_prvKey)  {
    // b64-decode encrypted_SSEencKey
    GByteArray encrypted_SSEencKey = {NULL, 0};
    encrypted_SSEencKey.data = g_base64_decode(b64_encrypted_SSEencKey, &encrypted_SSEencKey.len);
    /// @todo check if succeeded

    GByteArray encrypted_SSEverKey = {NULL, 0};
    encrypted_SSEverKey.data = g_base64_decode(b64_encrypted_SSEverKey, &encrypted_SSEverKey.len);
    /// @todo check if succeeded

    // unserialize CPABE public key
    GByteArray serialized_pubKey = {NULL, 0};
    serialized_pubKey.data = g_base64_decode(b64_serialized_pubKey, &serialized_pubKey.len);
    assert(serialized_pubKey.len > 800  && serialized_pubKey.len < 1000);
    /// @todo check if succeeded
    bswabe_pub_t *pub = bswabe_pub_unserialize(&serialized_pubKey, false);
    /// @todo check if succeeded

    // unserialize CPABE user/private key
    GByteArray serialized_prvKey = {NULL, 0};
    serialized_prvKey.data = g_base64_decode(b64_serialized_prvKey, &serialized_prvKey.len);
    /// @todo check if succeeded
    bswabe_prv_t *prv = bswabe_prv_unserialize(pub, &serialized_prvKey, false);
    /// @todo check if succeeded

    // decrypt both SSE keys
    printf("decrypting enc key...\n");
    char *sse_enc_key = cpabe_decrypt_buffer(pub, prv, &encrypted_SSEencKey);

    return NULL;

    printf("\ndecrypting ver key...\n");
    char *sse_ver_key = cpabe_decrypt_buffer(pub, prv, &encrypted_SSEverKey);
    return NULL;

    /// @todo remove '\n' from both results – btw: why?

    // free ressources
    bswabe_pub_free(pub);
    bswabe_prv_free(prv);
    g_byte_array_unref(&serialized_pubKey);
    g_byte_array_unref(&serialized_prvKey);
    g_byte_array_unref(&encrypted_SSEencKey);
    g_byte_array_unref(&encrypted_SSEverKey);

    // return both decrypted SSE keys
    char **retval = (char**) malloc(sizeof(char*) * 2);
    retval[0] = sse_enc_key;
    retval[1] = sse_ver_key;
    return retval;
}

