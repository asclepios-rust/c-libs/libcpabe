#pragma once

/** @brief decrypts some SSE keys that have been encrypted with CPABE
 *  @param b64_encrypted_SSEencKey  the encrypted SSE enc key (ciphertext is base64-encoded)
 *  @param b64_encrypted_SSEverKey  the encrypted SSE ver key (ciphertext is base64-encoded)
 *  @param pub                      serialized CPABE public key (base64-encoded)
 *  @param prv                      serialized CPABE private/user key (base64-encoded)
 *  @returns a list of exactly two decrypted SSE keys (SSEencKey and SSEverKey)
 */
char **getSSEKeys(char *b64_encrypted_SSEencKey, char *b64_encrypted_SSEverKey, char *b64_serialized_pubKey, char *b64_serialized_prvKey);
